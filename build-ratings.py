import MySQLdb as mdb
import time

from Glicko2 import Glicko2

dbh = mdb.connect('localhost','james','','taterset')
cur = dbh.cursor()
day_secs = 86400
now = time.time()
start = now-86400*365
glicko = Glicko2()

while start < now:
	cur.execute("""
		SELECT 
			matches.date,
			team1.team_name,
			matches.team_score_1,
			team2.team_name,
			matches.team_score_2
		FROM 
			matches
			INNER JOIN team AS team1 ON matches.team_1 = team1.id
			INNER JOIN team AS team2 ON matches.team_2 = team2.id
		WHERE date > %s AND date <= %s
		ORDER BY date ASC
		""",[
			time.strftime("%Y-%m-%d", time.gmtime(start)),
			time.strftime("%Y-%m-%d", time.gmtime(start+day_secs*10))
		]
	)

	for [date,team1,team1_score,team2, team2_score] in cur.fetchall():
		if team1 == team2:
			continue
		team1_score = int(team1_score)
		team2_score = int(team2_score)
		glicko.add_match({"scores":{team1 : team1_score, team2 : team2_score}, "date":date})
	glicko.calculate()
	start += day_secs*7
glicko.print_ratings(50)
ratings = glicko.get_ratings()
cur.execute("DELETE FROM ratings_overall;")
for team in ratings:
	cur.execute("""
		INSERT INTO ratings_overall 
		SELECT id, %s, %s, %s, 0
		FROM team 
		WHERE team_name=%s
		""", [
			ratings[team]["rating"], 
			ratings[team]["deviation"],
			ratings[team]["volatility"], 
			team
		]
	)
dbh.commit()

