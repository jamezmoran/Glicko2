#!/usr/bin/python3
# coding=latin-1

import MySQLdb as mdb
from lxml import html
import urllib.request
import re


headers = {
	"User-Agent" : "Mozilla/4.0 (compatible; MSIE 5.5; Windows NT)"
}
month_to_number = {
	'January' : '01',
	'February' : '02',
	'March' : '03',
	'April' : '04',
	'May' : '05',
	'June' : '06',
	'July' : '07',
	'August' : '08',
	'September' : '09',
	'October' : '10',
	'November' : '11',
	'December' : '12'
}
hltv_url = "https://www.hltv.org"
dbh = mdb.connect('localhost','james','','taterset')
cursor = dbh.cursor()

def scrape_tournament(url):
	request = urllib.request.Request(hltv_url+url, headers=headers)
	response = urllib.request.urlopen(request)
	if response.getcode() != 200:
		return None
	tree = html.fromstring(response.read())
	eventname = tree.xpath('//div[@class="eventname"]/text()')
	if eventname:
		[eventname] = eventname
	else:
		return None
	eventtype = tree.xpath('//span[@class="flag-align"]/text()')
	if eventtype:
		eventtype = 'ONLINE' if eventtype[0] == 'Online' else 'LAN'
	else:
		eventtype = 'LAN'
	cursor.execute("""
			INSERT INTO tournament (name, type, hltv_link, start_date) VALUES (%s,%s,%s,NULL)
			ON DUPLICATE KEY UPDATE hltv_link=%s
		""",
		[eventname, eventtype, url, url]
	)
	return eventname


for offset in range(10000,15000,100):
	print(str(offset))
	request = urllib.request.Request(hltv_url+"/results?offset="+str(offset), headers=headers)
	response = urllib.request.urlopen(request)
	if response.getcode() != 200:
		break	
	html_string = response.read()
	tree = html.fromstring(html_string)
	match_urls = tree.xpath('//div[@class="result-con"]/a[@class="a-reset"]/attribute::href')
	for match_url in match_urls:
		cursor.execute("SELECT * FROM matches WHERE hltv_link = %s", [match_url])
		if cursor.rowcount:
			continue
		request = urllib.request.Request(hltv_url+match_url, headers=headers)
		response = urllib.request.urlopen(request)
		if response.getcode() != 200:
			continue
		tree = html.fromstring(response.read())
		teamnames = tree.xpath('//div[@class="teamName"]/text()')
		maps = tree.xpath('//div[@class="mapname"]/text()')
		if not maps or 'Default' in maps:
			continue
		print(match_url)
		results = tree.xpath('//div[@class="results"]')

		[tournament_link] = tree.xpath('//div[@class="event text-ellipsis"]/a/attribute::href')
		if cursor.execute("SELECT * FROM tournament WHERE hltv_link=%s", [tournament_link]) == 0:
			scrape_tournament(tournament_link)

		scores = []
		for result in results:
			map_scores = result.xpath('span/text()')
			scores.append([ map_scores[0], map_scores[2] ])
		[date] = tree.xpath('//div[@class="date"]/text()')
		date_match = re.match("(\d\d?).. of (\w+) (\d\d\d\d)", date)
		date = date_match.group(3) + "-" + month_to_number[date_match.group(2)] + "-" + date_match.group(1)
		for i in range(len(scores)):
			for team in teamnames:
				cursor.execute("""
					INSERT INTO team (team_name) VALUES (%s)
					ON DUPLICATE KEY UPDATE id=id
				""", [team])
			cursor.execute("""
				INSERT INTO matches (team_1, team_score_1, team_2, team_score_2, date, map_name, tournament, hltv_link)
				SELECT t1.id, %s, t2.id, %s, %s, %s, t3.id, %s
				FROM
					(SELECT id FROM team WHERE team_name = %s) AS t1,
					(SELECT id FROM team WHERE team_name = %s) AS t2,
					(SELECT id FROM tournament WHERE tournament.hltv_link = %s) AS t3
			""", [scores[i][0], scores[i][1], date, "de_"+maps[i].lower(), match_url, teamnames[0], teamnames[1], tournament_link])
		dbh.commit()
		
