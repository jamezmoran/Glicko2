#!/usr/bin/python

# match defined as { scores : { teamname1 : ..., teamname2 : ... }, date : ..., type : ... }
# teams defined as { teamname : { rating : ..., deviation : ..., volatility : ... } }
#
#
import math

default_team = {
	"rating" : 1500,
	"deviation" : 350,
	"volatility" : 0.06
}
system_constant = 0.5

def _impact(deviation):
	return 1/math.sqrt(1 + 3*(deviation**2)/(math.pi**2))

def _expected_score(rating,opponent_rating,impact):
	return 1/(1+math.exp(-impact*(rating-opponent_rating)))

class Glicko2:
	def __init__(self,initial_teams=None):
		self.matches = [] 
		if initial_teams:
			self.teams = initial_teams
		else:
			self.teams = {}
	
	def calculate(self):
		if self.matches:
			new_stats = {}
			v_sums = {}
			d_sums = {}
			while len(self.matches):
				match = self.matches.pop()
				teams = list(match["scores"].keys())
				for x in range(2):
					[first_team_name, second_team_name] = teams

					first_team_score = match["scores"][first_team_name]
					second_team_score = match["scores"][second_team_name]

					score = 0
					score_diff = abs(first_team_score - second_team_score)
					if first_team_score > second_team_score:
						if score_diff < 3:
							score = 0.7
						elif score_diff < 5:
							score = 0.9
						else:
							score = 1
					elif first_team_score == second_team_score:
						score = 0.5
					else:
						if score_diff < 3:
							score = 0.3
						elif score_diff < 5:
							score = 0.1
						else:
							score = 0

					
					# Initialise teams
					if not first_team_name in v_sums:
						v_sums[first_team_name] = 0
					if not first_team_name in d_sums:
						d_sums[first_team_name] = 0

					if not first_team_name in self.teams:
						self.teams[first_team_name] = default_team.copy()
					if not first_team_name in new_stats:
						new_stats[first_team_name] = self.teams[first_team_name].copy()
						new_stats[first_team_name]["rating"] = (new_stats[first_team_name]["rating"]-1500)/173.7178
						new_stats[first_team_name]["deviation"] = new_stats[first_team_name]["deviation"]/173.7178

					if not second_team_name in v_sums:
						v_sums[second_team_name] = 0
					if not second_team_name in d_sums:
						d_sums[second_team_name] = 0

					if not second_team_name in self.teams:
						self.teams[second_team_name] = default_team.copy()
					if not second_team_name in new_stats:
						new_stats[second_team_name] = self.teams[second_team_name].copy()
						new_stats[second_team_name]["rating"] = (new_stats[second_team_name]["rating"]-1500)/173.7178
						new_stats[second_team_name]["deviation"] = new_stats[second_team_name]["deviation"]/173.7178

					first_team_rating = new_stats[first_team_name]["rating"]
					first_team_deviation = new_stats[first_team_name]["deviation"]
					second_team_rating = new_stats[second_team_name]["rating"]
					second_team_deviation = new_stats[second_team_name]["deviation"]

					impact = _impact(second_team_deviation)
					expected = _expected_score(first_team_rating, second_team_rating, impact)
					
					v_sums[first_team_name] += impact**2 * expected * (1 - expected)
					d_sums[first_team_name] += impact*(score - expected)

					teams.reverse()

			# Determine new volatility
			for team in new_stats:
				v = 1/v_sums[team]
				delta = v * d_sums[team]
				delta_2 = delta**2 
				deviation_2 = new_stats[team]["deviation"]**2
				volatility = new_stats[team]["volatility"]
				A = a = math.log(volatility**2)

				def f(x):
					exp_x = math.exp(x)
					return (exp_x*(delta_2-deviation_2-v-exp_x) /	(2*((deviation_2 + v + exp_x)**2))) - (x-a)/(system_constant**2)

				B = 0
				if delta_2 > deviation_2 + v:
					B = math.log(delta_2 - deviation_2 - v)
				else:
					k = 1
					while f(a - k*system_constant) < 0:
						k += 1
					B = a - k*system_constant

				fa, fb = f(A), f(B)
				convergence_tolerance = 0.000001
				while abs(B-A) > convergence_tolerance:
					C = A + (A - B)*fa / (fb-fa)
					fc = f(C)
					if fc*fb < 0:
						A, fa = B, fb
					else:
						fa /= 2
					B, fb = C, fc
				new_stats[team]["volatility"] = math.exp(A/2)

			for team in self.teams:
				if team in new_stats:
					new_pre_deviation = math.sqrt(new_stats[team]["deviation"]**2 + new_stats[team]["volatility"]**2)
					new_deviation = 1 / math.sqrt(1/(new_pre_deviation**2)+v_sums[team])
					new_rating = new_stats[team]["rating"] + (new_deviation**2) * d_sums[team]
					self.teams[team]["deviation"] = max(40,new_deviation*173.7178)
					self.teams[team]["rating"] = new_rating*173.7178 + 1500
					self.teams[team]["volatility"] = new_stats[team]["volatility"]
				else:
					self.teams[team]["deviation"] = math.sqrt((self.teams[team]["deviation"]/173.7178)**2 + self.teams[team]["volatility"]**2)*173.7178
			
	def add_match(self, match):
		self.matches.append(match)
	
	def print_ratings(self, max_rd):
		for team in self.teams:
			if self.teams[team]["deviation"] < max_rd:
				rating = self.teams[team]["rating"]
				deviation = self.teams[team]["deviation"]
				print(str(rating-2*deviation)+" "+str(deviation)+" "+team)
	
	def get_ratings(self):
		return self.teams;

