#!/usr/bin/python3

import MySQLdb as mdb
import re


dbh = mdb.connect('localhost','james','','taterset')
cursor = dbh.cursor()

cursor.execute("SELECT id, hltv_link FROM matches")
for [matchid, link] in cursor.fetchall():
	cursor.execute("UPDATE matches SET hltv_link=%s WHERE id=%s", [re.sub("/match/(\d+)-(.*)", "/matches/\g<1>/\g<2>", link), matchid])
dbh.commit()
